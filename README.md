![Build Status](https://gitlab.com/robinlinden/robinlinden.eu/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

# v1.robinlinden.eu
Old personal website I've adapted from plain HTML5 & CSS3 to using the Jekyll static website generator.

Live version at https://v1.robinlinden.eu/ if you're interested.